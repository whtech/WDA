<%@page import="com.farm.wda.util.AppConfig"%>
<%@page import="java.io.File"%>
<%@page import="com.farm.wda.Beanfactory"%>
<%@ page language="java" pageEncoding="utf-8"%>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><%=AppConfig.getString("config.web.title")%></title>
<script src="js/jquery11.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<style>
.nav li a {
	color: #ffffff;
}
</style>
<body style="background-color: #8a8a8a;">
	<jsp:include page="/commons/head.jsp"></jsp:include>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body text-center">
						<img alt="type" style="margin: auto;" class="img-responsive"
							src="img/type.png">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 20px;">
				<div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home"
							aria-controls="home" role="tab" data-toggle="tab">URL文件地址</a></li>
						<li role="presentation"><a href="#profile"
							aria-controls="profile" role="tab" data-toggle="tab">服务器文件目录</a></li>
						<li role="presentation"><a href="#messages"
							aria-controls="messages" role="tab" data-toggle="tab">附件上传</a></li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content"
						style="background-color: #ffffff; padding: 20px;">
						<div role="tabpanel" class="tab-pane active" id="home">
							<form action="path.jsp" method="post">
								<div class="form-group">
									<label for="exampleInputPassword1">文件URL地址</label> <input
										type="text" class="form-control" name="url"
										placeholder="如:http://www.xxxxx.com/Files/UploadFiles/file/1213-%9D%BF.doc">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">源文件类型</label> <select
										name="typename" class="form-control">
										<jsp:include page="commons/submitFileTypes.jsp"></jsp:include>
									</select>
								</div>
								<button type="submit" class="btn btn-primary">提交</button>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane" id="profile">
							<form action="path.jsp" method="post">
								<div class="form-group">
									<label for="exampleInputPassword1">关键字</label> <input
										type="text" class="form-control" name="key"
										placeholder="如:232212333">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">名称</label> <input
										type="text" class="form-control" name="filename"
										placeholder="如:资源文件">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">源文件地址(D:\doc\123123.doc)</label>
									<input type="text" class="form-control" name="path"
										placeholder="如:D:\doc\123123.doc">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">源文件类型</label> <select
										name="typename" class="form-control">
										<jsp:include page="commons/submitFileTypes.jsp"></jsp:include>
									</select>
								</div>
								<button type="submit" class="btn btn-primary">提交</button>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane" id="messages">
							<form action="path.jsp" method="post" enctype="multipart/form-data">

								<div class="form-group">
									<label for="exampleInputPassword1">关键字</label> <input
										type="text" class="form-control" name="key"
										placeholder="如:232212333">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">文件URL地址</label> <input 
										type="file" name="file" id="exampleInputFile">
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">源文件类型</label> <select
										name="typename" class="form-control">
										<jsp:include page="commons/submitFileTypes.jsp"></jsp:include>
									</select>
								</div>
								<button type="submit" class="btn btn-primary">提交</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>